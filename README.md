# Test uso basico de nodejs e introducción al sistema de crawling que usamos actualmente. #

## Instalar dependencias ##

npm install

## Ejercicios ##

### 1. Completar el modulo en ./parser.js que se encargar de procesar los datos (./test/aerolineaspayaso-in.json) en una respuesta valida (./test/aerolineaspayaso-out.json) ###

  El unico archivo que puede modificarse es el ./parser.js

Para considerar la prueba como valida tienen que pasarse todos los test de ./test/test.parser.js, para
correr los test hay que usar mocha, despues de hacer "npm install" hay dos posibles comandos;

npm run watch

npm run test

### 2. Utilizando los datos de ejemplo de búsqueda (./test/ejemplo-busqueda.json) realizar: ###

  * Un filtro por proveedores. Traer solo los itinerarios cuyos providers queden dentro del filtro. Si al filtrar el itinerario se queda sin proveedores, no traer el itinerario. Si el bestPrice inicial queda fuera del filtro, modificar el bestPrice con el nuevo valor.
  * Un filtro por precio. Traer solo los itinerarios cuyos precios queden dentro del filtro. Si al filtrar el itinerario se queda sin providers que satisfagan el filtro de precios, no traer el itinerario. Si el bestPrice inicial queda fuera del filtro, modificarlo el bestPrice con el nuevo valor.
  * Ordenar los resultados por duración de menor a mayor.
  * Ordenar los resultados por precio tomando en cuenta el bestPrice de menor a mayor.

Este ejercicio no cuenta con tests.

## Descripcion de los archivos ##

* ./README: Readme
* ./test/aerolineaspayaso-in.json: Json de resultados sin parsear (respuesta cruda de aerolineaspayaso)
* ./test/aerolineaspayaso-out.json: Json de salida (resultados parseados)
* ./test/ejemplo-busqueda.json: Json de ejemplo de busqueda
* ./index.js: Objecto Crawler
* ./parser.js: Modulo de parser (Ejercicio 1)
* ./busqueda.js: Modulo para filtros y ordenamiento (Ejercicio 2)
* ./test/test.parser.js: Tests que hay que pasar del Ejercicio 1

## Respecto al formato crudo de entrada ( ./test/aerolineaspayaso-in.json )

Cada item en este array representa un conjunto de vuelos que tienen el mismo precio y url.
Adentro de cada item del array un conjunto de segmentos de vuelos de ida y de vuelos de
regreso, la combitoria de todos estos representa un vuelo.
Los vuelos pueden estar repetidos, se pide responder con los vuelos sin
repetir y ordenados por precio y duración.

## EJEMPLO DE UN ITEM (algunos atributos fueron eliminados por comodida):

```{
  "providerName": "AEROLINEASPAYASO.COM",
  "price": [{ "value": 6171.2, "currency": "ARS" }],
  "bookURL": "https://www.aerolineaspayaso.com/vuelos/RT/BUE_RIO/27-07-2015_06-08-2015/1/0/0/Economy/G3",
  "flights": [
    {
      "segments": [
        {
          "RefNumber": "0",
          "duration": "30",
          "flights": [{ vuelo-ida-A1 }]
        },
        {
          "RefNumber": "1",
          "duration": "60",
          "flights": [{ vuelo-vta-A1 }, { vuelo-vta-A2 }]
        },
        {
          "RefNumber": "1",
          "duration": "90",
          "flights": [{ vuelo-vta-B1 }, { vuelo-vta-B2 }]
        }
      ],
      "airline": "G3"
    },
    {
      "segments": [
        {
          "RefNumber": "0",
          "duration": "130",
          "flights": [{ vuelo-ida-C1 }]
        },
        {
          "RefNumber": "1",
          "duration": "60",
          "flights": [{ vuelo-vta-C1 }]
        },
      ],
      "airline": "G3"
    }
    {
      "segments": [
        {
          "RefNumber": "0",
          "duration": "130",
          "flights": [{ vuelo-ida-C1 }]
        },
        {
          "RefNumber": "1",
          "duration": "60",
          "flights": [{ vuelo-vta-C1 }]
        },
      ],
      "airline": "G3"
    }
  ]
},

Salida
{
  "price": [{ "value": 6171.2, "currency": "ARS" }],
  "bookURL": "https://www.aerolineaspayaso.com/vuelos/RT/BUE_RIO/27-07-2015_06-08-2015/1/0/0/Economy/G3",
  "segments": {
    duration: 30 + 60,
    outbound: [{ vuelo-ida-A1 }],
    inbound: [{ vuelo-vta-A1 }, { vuelo-vta-A2 }],
  },
},
{
  "price": [{ "value": 6171.2, "currency": "ARS" }],
  "bookURL": "https://www.aerolineaspayaso.com/vuelos/RT/BUE_RIO/27-07-2015_06-08-2015/1/0/0/Economy/G3",
  "segments": {
    duration: 30 + 90,
    outbound: [{ vuelo-ida-A1 }],
    inbound: [{ vuelo-vta-B1 }, { vuelo-vta-B2 }]
  },
},
{
  "price": [{ "value": 6171.2, "currency": "ARS" }],
  "bookURL": "https://www.aerolineaspayaso.com/vuelos/RT/BUE_RIO/27-07-2015_06-08-2015/1/0/0/Economy/G3",
  "segments": {
    duration: 130 + 90,
    outbound: [{ vuelo-ida-C1 }],
    inbound: [{ vuelo-vta-C1 }]
  },
},
