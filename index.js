const fs = require('fs');
const EventEmitter = require('events');
const moment = require('moment');
const aerolineaspayasoParser = require('./parser.js');

class Crawler extends EventEmitter {
  constructor(searchRequest) {
    super();
    this.doSearch(searchRequest);
  }

  doSearch(_searchRequest) {
    const searchRequest = _searchRequest;
    // console.log(params);
    // Viene DD/MM/YYYY y debe buscarse como yyyyMMdd
    const outboundDate = moment(searchRequest.outboundDate, 'YYYY-MM-DD');

    if (searchRequest.inboundDate) {
      const inboundDate = moment(searchRequest.inboundDate, 'YYYY-MM-DD');
      searchRequest.inboundDate = inboundDate.format('YYYY-MM-DD');
    }

    searchRequest.outboundDate = outboundDate.format('YYYY-MM-DD');

    this.instance = searchRequest;

    this.URLTRACKINGPARAMS = `utm_medium=metasearch&utm_source=turismocity&utm_campaign=${searchRequest.fromLocation}-${searchRequest.toLocation}&utm_term=flight`;

    this.doPull();
  }

  doPull() {
    const filename = './test/aerolineaspayaso-in.json';
    const body = JSON.parse(fs.readFileSync(filename, 'utf8'));

    const results = aerolineaspayasoParser(body, this.URLTRACKINGPARAMS);
    setTimeout(() => {
      this.emit('pulled', results);
      this.emit('ready');
    }, 100);
  }
}

exports.Crawler = Crawler;
