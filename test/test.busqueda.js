const { expect } = require('chai');
const { sort, priceFilter, providersFilter } = require('../busqueda');
const testItineraries = require('./ejemplo-busqueda.json');

describe('Turismocity filtros y ordenamientos', () => {
  it('should return itineraries containing the specified providers', () => {
    const providers = ['Avantrip', 'GotoGate'];
    const filteredResult = providersFilter(testItineraries, providers);
    const hasMatch = filteredResult.itineraries[0].providers.some((prov) => providers.includes(prov.providerName));

    expect(filteredResult).to.be.an('object');
    expect(filteredResult.itineraries).to.be.an('array');
    expect(hasMatch).to.be.true;
  });

  it('should return itineraries between minPrice and maxPrice', () => {
    const filteredResult = priceFilter(testItineraries, 3000, 3008);
    expect(filteredResult).to.be.an('object');
    expect(filteredResult.itineraries).to.be.an('array');
    expect(filteredResult.itineraries.length).to.be.equal(3);
  });

  it('should return itineraries sorted by price from min to max', () => {
    const sortedResult = sort(testItineraries, 'price');
    const lastItem = sortedResult.itineraries[sortedResult.itineraries.length - 1];
    const firstItem = sortedResult.itineraries[0];

    expect(sortedResult).to.be.an('object');
    expect(sortedResult.itineraries).to.be.an('array');
    expect(firstItem.bestPrice.price.value < lastItem.bestPrice.price.value).to.be.true;
  });
});
