/* global it */
/* global before */
/* global describe */
const expect = require('chai').expect;
const assert = require('chai').assert;
const glob = require('glob');
const { CLIEngine } = require('eslint');


const AerolineasPayasoCrawler = require('../index.js').Crawler;

const searchParams = {
  countryCode: 'AR',
  fromLocation: 'BUE',
  toLocation: 'RIO',
  cabinClass: 'Economy',
  outboundDate: '2015-7-27',
  inboundDate: '2015-8-6',
  numAdults: 1,
  numChildren: 0,
  tripType: 'RoundTrip',
  numInfants: 0,
};

const fs = require('fs');
const expected = { itineraries: JSON.parse(fs.readFileSync('./test/aerolineaspayaso-out.json', 'utf8')) };

const paths = glob.sync('./parser.js');
const engine = new CLIEngine({
  envs: ['node', 'mocha'],
  useEslintrc: true,
});

let results = engine.executeOnFiles(paths).results;

function formatMessages(messages) {
  const errors = messages.map((message) => `${message.line}:${message.column} ${message.message.slice(0, -1)} - ${message.ruleId}\n`);

  return `\n${errors.join('')}`;
}

// function generateTest(result) {
//   const { filePath, messages } = result;
//
//   it(`validates ${filePath}`, () => {
//     if (messages.length > 0) {
//       assert.fail(false, true, formatMessages(messages));
//     }
//   });
// }
//
// describe('ESLint', () => {
//   results.itineraries.forEach((result) => generateTest(result));
// });


describe('Turismocity simple parser', () => {
  before((done) => {
    const crawler = new AerolineasPayasoCrawler(searchParams);

    crawler.on('pulled', (data) => {
      results = data;
    })
    .on('ready', done);
  });

  it('should have itineraries', () => {
    assert.equal(results.itineraries.length, expected.itineraries.length,
			'Cantidad de resultados no es la esperada');
  });

  it('should be ordered by price and duration', () => {
    const prices = results.itineraries.map(e => e.providers[0].price.value);
    const durations = results.itineraries.map(e => e.segments.duration);
    let sorted = true;
    for (let i = 0; i < prices.length - 1; i++) {
      if (prices[i] > prices[i + 1]) {
        sorted = false;
        break;
      }
      if (prices[i] === prices[i + 1] && durations[i] > durations[i + 1]) {
        sorted = false;
        break;
      }
    }
    assert.ok(sorted, 'itineraries in order');
  });

  it('should be the first result the same', () => {
    expect(expected.itineraries[0]).to.deep.equal(results.itineraries[0]);
  });

  it('should like expected response itinerary', () => {
    for (let i = 0; i < expected.itineraries.length; i += 1) {
      expect(expected.itineraries[i]).to.deep.equal(results.itineraries[i], 'error itinerario #' + i);
    }
  });
});
