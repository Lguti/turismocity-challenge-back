const testItineraries = require('./test/ejemplo-busqueda.json');

/**
 *
 * @param {Array} segments
 * @returns number
 * @description Get the total duration of all flight segments
 */
function getTotalDuration(segments) {
  const totalDuration = segments.reduce((acc, segment) => acc + segment.duration, 0);

  return totalDuration;
}

/**
 *
 * @param {Array} itineraries
 * @param {String[]} providers
 * @returns Object
 * @description Itineraries that containing the name in param providers, example provider = ['Avantrip', 'GotoGate']
 */
function providersFilter(itineraries, providers) {
  const lowerCaseProviders = providers.map((prov) => prov.toLowerCase());
  const result = itineraries.filter((itinerary) => {
    const itineraryProviders = itinerary.providers.map(({ providerName }) => providerName.toLowerCase());
    return itineraryProviders.some((itinerariesProvider) => lowerCaseProviders.includes(itinerariesProvider));
  });

  return { itineraries: result };
}

/**
 *
 * @param {Array} itineraries
 * @param {Number} minPrice
 * @param {Number} maxPrice
 * @returns Object
 * @description Itineraries between minPrice and maxPrice
 */
function priceFilter(itineraries, minPrice, maxPrice) {
  const result = itineraries.filter((itinerary) => itinerary.providers.some((prov) => {
    const price = prov.price.value;
    return price >= minPrice && price <= maxPrice;
  }));

  return { itineraries: result };
}

/**
 *
 * @param {Array} itineraries
 * @param {String} sortType
 * @returns Object
 * @description Itineraries sorted by sortType "duration" or "price", default sort by "duration"
 */
function sort(itineraries, sortType = 'duration') {
  if (sortType.toLowerCase() === 'price') {
    const sortedByPrice = itineraries.sort((a, b) => a.bestPrice.price.value - b.bestPrice.price.value);
    return { itineraries: sortedByPrice };
  }

  const sortedByDuration = itineraries.sort((a, b) => getTotalDuration(a.segments) - getTotalDuration(b.segments));
  return { itineraries: sortedByDuration };
}

module.exports = {
  sort,
  priceFilter,
  providersFilter,
};
// Ejemplos de como llamar a las funciones
providersFilter(testItineraries, ['GotoGate', 'Avantrip']);
priceFilter(testItineraries, 2000, 5000);
sort(testItineraries);
// sort(testItineraries, 'price');
