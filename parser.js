/**
 *
 * @param {Array} flights
 * @returns Array
 * @description Remove duplicate flights
 */
function removeDuplicateFlights(flights) {
  return flights.reduce((acc, flight) => {
    const hasEqual = acc.find((f) => f.duration === flight.duration);
    if (!hasEqual) {
      acc.push(flight);
    }

    return acc;
  }, []);
}

/**
 *
 * @param {Array} flights
 * @returns Object
 * @description Return needed attributes: flightNumber, departure, arrival, airline, airplane, departureTime, arrivalTime.
 */
function segmentsFlightsAttributeFormatter(flights) {
  return flights.map(({
    FlightNumber, Equipment, ArrivalDateTime, ArrivalAirportLocationCode, DepartureDateTime, DepartureAirportLocationCode, OperatingAirlineCode,
  }) => ({
    flightNumber: FlightNumber,
    departure: DepartureAirportLocationCode,
    arrival: ArrivalAirportLocationCode,
    airline: OperatingAirlineCode,
    airplane: Equipment,
    departureTime: DepartureDateTime,
    arrivalTime: ArrivalDateTime,
  }));
}

/**
 *
 * @param {Object} outboundflight
 * @param {Object} inboundflight
 * @returns Object
 * @description Create segments output object
 */
function createCombination(outboundflight, inboundflight) {
  return {
    duration: Number(outboundflight.duration) + Number(inboundflight.duration),
    outbound: {
      flights: segmentsFlightsAttributeFormatter(outboundflight.flights),
    },
    inbound: {
      flights: segmentsFlightsAttributeFormatter(inboundflight.flights),
    },
  };
}

/**
 *
 * @param {Array} flights
 * @returns Array
 * @description Formatter each segments and return posibles go and back flights
 */
function formatSegments(flights) {
  const combinations = [];
  const outboundSegments = [];
  const inboundSegments = [];

  flights.forEach((flight) => {
    outboundSegments.push(...flight.segments.filter((segm) => segm.RefNumber === '0'));
    inboundSegments.push(...flight.segments.filter((segm) => segm.RefNumber === '1'));
  });

  const uniqueOutboun = removeDuplicateFlights(outboundSegments);
  const uniqueInbound = removeDuplicateFlights(inboundSegments);

  uniqueOutboun.forEach((outboundflight) => {
    uniqueInbound.forEach((inboundflight) => {
      const combination = createCombination(outboundflight, inboundflight);
      combinations.push(combination);
    });
  });

  return combinations;
}

/**
 *
 * @param {Array} flightFormatted
 * @returns Array
 * @description Order flights by prices and duration
 */
function sortByPriceAndDuration(flights) {
  return flights.sort((a, b) => {
    if (a.providers[0].price.value !== b.providers[0].price.value) {
      return a.providers[0].price.value - b.providers[0].price.value;
    }

    const durationA = a.segments.duration;
    const durationB = b.segments.duration;

    return durationA - durationB;
  });
}

module.exports = function parseResponse(response, trackingParams) {
  const flightFormatted = [];
  response.forEach((item) => {
    const provider = {
      providerName: item.providerName,
      bookURL: `${item.bookURL}?${trackingParams}`,
      price: {
        value: item.price[0].value,
        currency: item.price[0].currency,
      },
    };

    const segments = formatSegments(item.flights);

    segments.forEach((segm) => {
      const parseItem = {
        providers: [provider],
        segments: {
          ...segm,
        },
      };
      flightFormatted.push(parseItem);
    });
  });

  return { itineraries: sortByPriceAndDuration(flightFormatted) };
};
